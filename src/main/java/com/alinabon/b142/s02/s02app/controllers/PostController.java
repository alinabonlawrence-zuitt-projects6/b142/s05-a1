package com.alinabon.b142.s02.s02app.controllers;

import com.alinabon.b142.s02.s02app.models.Post;
import com.alinabon.b142.s02.s02app.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    //Create a new post
    @RequestMapping(value = "users/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String token, @RequestBody Post newPost) {
        postService.createPost(newPost, token);
        return new ResponseEntity<>("New post was created", HttpStatus.CREATED);
    }

    //Retrieve all post
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    //Update an existing post
    @RequestMapping(value = "users/posts/{postId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@RequestHeader(value = "Authorization") String token, @PathVariable Long postId, @RequestBody Post updatedPost) {
        return postService.updatePost(postId, updatedPost, token);
    }

    //Delete a post
    @RequestMapping(value = "posts/{id}", method = RequestMethod.DELETE)
    public  ResponseEntity<Object> deletePost(@PathVariable Long id, @RequestHeader(value = "Authorization") String token) {
        return postService.deletePost(id, token);
    }

    //Retrieve all post by an existing user
    @RequestMapping(value = "/my-posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@RequestHeader(value = "Authorization") String token) {
        return new ResponseEntity<>(postService.getMyPosts(token), HttpStatus.OK);
    }
}
