package com.alinabon.b142.s02.s02app.services;

import com.alinabon.b142.s02.s02app.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    void createPost(Post newPost, String token);
    ResponseEntity updatePost(Long postId, Post existingPost, String token);
    ResponseEntity deletePost(Long id, String token);
    Iterable<Post> getPosts();
    Iterable<Post> getMyPosts(String token);
}
