package com.alinabon.b142.s02.s02app.services;

import com.alinabon.b142.s02.s02app.models.User;

import java.util.Optional;

public interface UserService {
    Iterable<User> getUser();
    void createUser(User newUser);
    void updateUser(Long id, User existingUser);
    void deleteUser(Long id);
    Optional<User> findByUsername(String username);
}
